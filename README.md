KeyguardTest
===================
端末のロック状態、KeyguardManager のステータスを調査。
```java
KeyguardManager keyguardManager =
        (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
keyguardManager#isDeviceLocked() // API Level 22
keyguardManager#isKeyguardLocked() // API Level 16
keyguardManager#isKeyguardSecure() // API Level 23
```

### ロック設定なし
```java
// 電源押下(画面OFF)
action=android.intent.action.SCREEN_OFF
deviceLocked=false
keyguardLocked=false
keyguardSecure=false

// 電源押下(画面ON)
action=android.intent.action.USER_PRESENT
deviceLocked=false
keyguardLocked=false
keyguardSecure=false
```

### ロック設定あり(パターン)
```java
// 電源押下(画面OFF)
action=android.intent.action.SCREEN_OFF
deviceLocked=true
keyguardLocked=true
keyguardSecure=true

// 電源押下(画面ON)->パターン解除後
action=android.intent.action.USER_PRESENT
deviceLocked=false
keyguardLocked=false
keyguardSecure=true
```

### ロック設定あり(スワイプ)
```java
// 電源押下(画面OFF)
action=android.intent.action.SCREEN_OFF
deviceLocked=false
keyguardLocked=true
keyguardSecure=false

// 電源押下(画面ON)->スワイプ後
action=android.intent.action.USER_PRESENT
deviceLocked=false
keyguardLocked=false
keyguardSecure=false
```
