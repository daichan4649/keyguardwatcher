package daichan4649.watcher;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PreferenceUtil {

    private enum PrefKey {
        COUNT_UNLOCK("count_unlock"),;

        private final String key;

        private PrefKey(String key) {
            this.key = key;
        }

        public String getKey() {
            return key;
        }
    }

    public static int getUnlockCount(Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getInt(PrefKey.COUNT_UNLOCK.getKey(), 0);
    }

    public static void countUpUnlock(Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        int now = pref.getInt(PrefKey.COUNT_UNLOCK.getKey(), 0);
        pref.edit().putInt(PrefKey.COUNT_UNLOCK.getKey(), ++now).commit();
    }

    public static void resetUnlockCount(Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        pref.edit().putInt(PrefKey.COUNT_UNLOCK.getKey(), 0).commit();

    }
}
