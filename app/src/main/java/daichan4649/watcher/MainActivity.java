package daichan4649.watcher;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private TextView mCountView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mCountView = (TextView) findViewById(R.id.count);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshCountView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.action_start:
                startKeyguardService();
                return true;
            case R.id.action_stop:
                stopKeyguardService();
                return true;
            case R.id.action_refresh:
                refreshCountView();
                return true;
            case R.id.action_reset:
                resetCount();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Keyguard関係
    private void startKeyguardService() {
        Intent service = new Intent(MainActivity.this, KeyguardWatcher.class);
        startService(service);
    }

    private void stopKeyguardService() {
        Intent service = new Intent(MainActivity.this, KeyguardWatcher.class);
        stopService(service);
    }

    private void refreshCountView() {
        int unlockCount = PreferenceUtil.getUnlockCount(this);
        Log.d(TAG, "unlockCount=" + unlockCount);
        mCountView.setText(String.format("unlock=%d", unlockCount));
    }

    private void resetCount() {
        PreferenceUtil.resetUnlockCount(this);
        refreshCountView();
    }
}
