package daichan4649.watcher;

import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.text.TextUtils;
import android.util.Log;

public class KeyguardWatcher extends Service {

    private static final String TAG = KeyguardWatcher.class.getSimpleName();

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        super.onCreate();
        registerKeyguardReceiver(getApplicationContext());
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
        unregisterKeyguardReceiver(getApplicationContext());
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void registerKeyguardReceiver(Context context) {
        Log.d(TAG, "registerKeyguardReceiver");
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_USER_PRESENT);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        context.registerReceiver(mKeyguardReceiver, filter);
    }

    private void unregisterKeyguardReceiver(Context context) {
        Log.d(TAG, "unregisterKeyguardReceiver");
        try {
            context.unregisterReceiver(mKeyguardReceiver);
        } catch (Exception e) {
            // 何もしない
        }
    }

    private BroadcastReceiver mKeyguardReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            Log.d(TAG, "action=" + action);
            showKeyguardStatus(context);

            // キーガード状態判定
            if (TextUtils.equals(action, Intent.ACTION_USER_PRESENT)) {
                // Activity#onResume はロック中に画面ONにしただけでも飛んでくるため、
                // ロック中かどうかの判定はこのintentで判断する必要あり

                PreferenceUtil.countUpUnlock(context);

                int count = PreferenceUtil.getUnlockCount(context);
                showNotification(context, count);

            } else if (TextUtils.equals(action, Intent.ACTION_SCREEN_OFF)) {
                // スクリーンOFF＝ロック中
            }
        }
    };

    private static void showKeyguardStatus(Context context) {
        KeyguardManager keyguardManager =
                (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);

        Log.d(TAG, String.format("deviceLocked=%b, keyguardLocked=%b, keyguardSecure=%b",
                keyguardManager.isDeviceLocked(), // API Level 22
                keyguardManager.isKeyguardLocked(), // API Level 16
                keyguardManager.isKeyguardSecure())); // API Level 23
    }

    private static final int ID_UNLOCK = 100;

    private static void showNotification(Context context, int unlockCount) {
        int notificationId = ID_UNLOCK;
        String contentTitle = "unlock";
        String contentText = String.format("%d", unlockCount);

        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context);

//        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle(contentTitle);
        builder.setContentText(contentText);
        builder.setContentIntent(pendingIntent);
//        long[] vibratePattern = new long[]{500, 500, 500};
//        builder.setVibrate(vibratePattern);

        NotificationManagerCompat nm = NotificationManagerCompat.from(context);
        nm.notify(notificationId, builder.build());
    }
}
